package io.piano.c2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.cloud.stream.messaging.Sink
import org.springframework.kafka.annotation.EnableKafka


@SpringBootApplication
@EnableBinding(Sink::class)
@EnableKafka
class RoomviewApplication

fun main(args: Array<String>) {
    runApplication<RoomviewApplication>(*args) {
        //		setBannerMode(Banner.Mode.OFF)
//		setBanner(Banner())
    }
}
