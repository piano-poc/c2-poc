package io.piano.c2

import com.google.common.base.Stopwatch
import org.jetbrains.kotlin.script.jsr223.KotlinJsr223JvmLocalScriptEngineFactory
import org.junit.Test
import javax.script.ScriptEngineManager
import kotlin.script.experimental.host.toScriptSource
import kotlin.script.experimental.jvm.defaultJvmScriptingHostConfiguration
import kotlin.script.experimental.jvmhost.BasicJvmScriptEvaluator
import kotlin.script.experimental.jvmhost.BasicJvmScriptingHost
import kotlin.script.experimental.jvmhost.JvmScriptCompiler
import kotlin.script.experimental.jvmhost.createJvmCompilationConfigurationFromTemplate
import kotlin.script.templates.standard.SimpleScriptTemplate

class EblExecution {


    @Test
    fun jsTest() {
        val engine = ScriptEngineManager().getEngineByName("nashorn")
        val stopwatch = Stopwatch.createUnstarted()

        for (i in 0..100) {
            stopwatch.reset()

            stopwatch.start()
            engine.eval("x = 10 + 10")
        }
        println(stopwatch)
        assert(stopwatch.elapsed().toMillis() < 5, {stopwatch} )

    }

    @Test
    fun kotlinScriptingHost() {
        val script = """
        val x = 10 + 10
        println("omega")
    """
        val compiler = JvmScriptCompiler()
        val evaluator = BasicJvmScriptEvaluator()

        val host = BasicJvmScriptingHost(compiler = compiler, evaluator = evaluator)
        val config = createJvmCompilationConfigurationFromTemplate<SimpleScriptTemplate>(defaultJvmScriptingHostConfiguration)
        val stopwatch = Stopwatch.createUnstarted()
        val script1 = script.toScriptSource()

        for (i in 0..100) {
            stopwatch.reset()

            stopwatch.start()

            val result = host.eval(script1, config, null)
            println(result)
            println(stopwatch)

        }
        println(stopwatch)
        assert(stopwatch.elapsed().toMillis() < 5, {stopwatch} )
    }

    @Test
    fun testKotlinScriptEngine() {
        val engine = KotlinJsr223JvmLocalScriptEngineFactory().scriptEngine//ScriptEngineManager().getEngineByExtension("kts")!!
        val stopwatch = Stopwatch.createUnstarted()

        for (i in 0..100) {
            stopwatch.reset()
            stopwatch.start()
            engine.put("z", 33)
            engine.eval("""val x = 10 + bindings["z"] as Int""")
        }
        println(stopwatch)
        assert(stopwatch.elapsed().toMillis() < 5, {stopwatch} )
    }
}

data class LawyerType(val age: Int, var name: String)
